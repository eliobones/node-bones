<aside>
  <dl>
  <dd>Who stooping opened my left side, and took</dd>
  <dd>From thence a rib, with cordial spirits warm,</dd>
  <dd>And life-blood streaming fresh</dd>
</dl>
</aside>

Rest API in [NodeJs](https://nodejs.org/) with zero dependencies. It serves as a way to learn **NodeJs** and to demonstrate the anatomy of an **eliobones** API (as needed to support applications and solutions), the **elioWay**.

This is not intended to be used as a production API. It is not properly secure nor robust; nor is it tested well in the field. It could be used as a development API, but I suggest you try one of the other NodeJs based **eliobones** APIs written on frameworks like ExpressJs, FeathersJs, KeystoneJs.

Usefully though, any eliobones API can be swapped for another - and your client will need no changes. They all offer the same endpoints and accept the same data schema... that's one of the reasons we do [Things](https://schema.org/Thing), the **elioWay**.

Ideally students of NodeJs, interested in doing Things the **elioWay**, might like to experiment with this project - free as it is from any other dependencies. If so, this **eliobones** flavour is based on lessons gleaned by following **Pirple**'s [Nodejs Master Class](https://www.pirple.com/courses/take/the-nodejs-master-class). I highly recommend you take this course as well.
