# Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/bones.git
cd bones
git clone https://gitlab.com/bones/node-bones.git
```

## TODOS

1. Have **node-bones** consume **bones**. **bones** was an **expressjs** server with **mongo** and moved to **mongoose-bones**. The core application endpoints for each _rib_, written in **node-bones**, was moved to **bones**. The core db layer in **node-bones** was separated out as well. Now we need to consume those libraries, leaving only the server parts of this elioWay API.
