# node-bones Credits

## Core Thanks!

- [NodeJs](https://nodejs.org/)
- [pirple.com](https://www.pirple.com/courses/take/the-nodejs-master-class)

## Artwork

- [Ryan_Dahl](https://upload.wikimedia.org/wikipedia/commons/b/b2/Ryan_Dahl.jpg)
