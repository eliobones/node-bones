# Installing node-bones

## Prerequisites

- [Prerequisites (but there aren't any)](/eliobones/node-bones/prerequisites.html)

## Installation

**WARNING**

**node-bones** is primarily intended for learning **NodeJs**, the **elioWay**. It's a fully featured and working _and_ tested prototype, which admitedly is not hardened for production use. The former relates to the latter to the extent that it tempts you to deploy it for production use.

_"Fully featured" because **elioWay** apps have few features spelled out by the letters of its name **e l i o** and these few features are fully featured._

But if you must...

1. Open `bones/config.js`.
2. Change the `production: hashingSecret` to something unique and complicated. Use a password generator.
3. Change the `host` to match the domain you are serving the API on.
4. Save and upload this fileset to your server.
5. Terminal into your server, `cd` into the root folder of the fileset.
6. Launch with `NODE_ENV=production node bones/index.js`.

Alternatively, if your host supports CI, add a `package.json` file and include a `scripts` key with (something like):

```json
...
"scripts": {
  "serve": "NODE_ENV=production node bones/index.js"
},
...
```

Check your hosting company's website, they might launch a **NodeJs** application from `run` or `start` instead.

If they automatically call the `build` process, you can simply add a benign command for that key, because no build is required for this application:

```json
...
"scripts": {
  "serve": "NODE_ENV=production node bones/index.js",
  "build": "node -v"
},
...
```
