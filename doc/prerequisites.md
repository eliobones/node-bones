# node-bones Prerequisites

The only prerequisite is NodeJs 14+.

## Recommended

Sign up and follow Pirple's [NodeJs Master Class](https://www.pirple.com/courses/take/the-nodejs-master-class).
