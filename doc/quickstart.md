# Quickstart node-bones

- [node-bones Prerequisites](/eliobones/node-bones/prerequisites.html)
- [Installing node-bones](/eliobones/node-bones/installing.html)

## Nutshell

- WTF is **node-bones** and the **elioWay**.
- Overview of API Anatomy.
- Start the server.
- Call its endpoints.

## WTF

All data is modelled on [SchemaOrg](https://Schema.org) `Thing`s ("dropping the pretence one `Thing` is so different from another `Thing`").

To do `Things` the **elioWay**, apps follow a simple workflow:

- **E**ngage a `Thing`.
- **L**ist `Thing`s belonging to the Engaged `Thing`.
- **I**terate by Engaging with any `Thing` which is Listed.
- **O**ptimize by adding missing features and filters required by the application you are writing.

In light of this, all **eliobones** APIs support:

- Standard CRUD operations on an Engaged `Thing` (where we call "Create" => "Takeup")... **Engage** is what the **E** stands for.
- Auth operations to Login and Logout of an Engaged `Thing`.
- ViewList/Enlist/Unlist/New+Enlist (which we call "Takeon") operations on an Engaged `Thing`'s List... **List** is what the **L** stands for.
- A Schema endpoint to return the fields documented in [SchemaOrg](https://Schema.org) of any given _ThingType_.

To perform CRUD operations on an item in a **List**, you must first **Engage** it... and so on... **Iterate** is what the **I** stands for.

Any features you need, which aren't currently offered or supported by **node-bones**, can be custom made, using Schema where possible, to enhance and enrich the **elioWay** universe for future developers. **Optimize** is what the **O** stands for.

## API Anatomy

The **node-bones** API mirrors the anatomy of all **eliobones** API libraries. We call these its _ribs_.

### Auth

- **loginT** `POST` `/auth/:T` Exchange a username and password for a Permit. _Unprotected._
- **logoutT** `DELETE` `/auth/:T/:id` Expire the Permit.

### Engage

- **takeupT** `POST` `/:T` "Takeup" a Thing of _schemaType_ `:T`, e.g. "Thing", "MedicalCondition", "Vehicle". _Unprotected._
- **updateT** `PATCH` `/:T/:id` Update an _engaged_ Thing.
- **readT** `GET` `/:T/:id` View the _engaged_ Thing.
- **destroyT** `DELETE` `/:T/:id` Delete an _engaged_ Thing (doesn't delete any Things in its _list_).

### List

- **takeonT** `POST` `/list/:T/:id/:LT` "Takeup" then "TakeOn" a new Thing of _schemaType_ `:LT` and add it to the _engaged_ Thing's _list_.
- **listT** `GET` `/list/:T/:id` View the _engaged_ Thing's _list_.
- **listOfT** `GET` `/list/:T/:id/:LT` View the _engaged_ Thing's _list_ of _schemaType_ `:LT`.
- **enlistT** `PUT` `/list/:T/:id/:LT/:lid` En _list_ an existing Thing.
- **unlistT** `DELETE` `/list/:T/:id/:LT/:lid` Un _list_ an existing Thing.

### Schema

- **schemaOfT** `GET` `/schema/:T` Get the Meta Schema of _schemaType_ `:T`. _Unprotected._
- **schemaT** `GET` `/schema/:t` Get an empty instance of _schemaType_ `:t`. _Unprotected._

See the [elioWay Documentation](https://elioway.gitlab.io) for more details about the project. (NB: Very much a work in progress!)

## Start the server

In the library's root folder type `node bones/index.js`.

## Call its endpoints

### Scripted

- See our "out-of-the-box" client calls, which mirror the API, in the `client/calls` folder.
- Take a look at, and call the `test/testClientCalls.js` script, which cycles through the endpoints.

### Curling

**Schema**

```shell
# schemaT: Meta with Field Types.
curl -X GET http://localhost:5000/schema/Thing
# schemaT: Blank, new instance.
curl -X GET http://localhost:5000/schema/thing
```

**Engage**

```shell
# Engage/takeupT
curl -X POST http://localhost:5000/Thing -d '{ "name": "God", "password": "letmein" }'
# Auth/loginT
curl -X POST http://localhost:5000/auth/Thing -d '{ "name": "God", "password": "letmein" }'
# Using `identifier` from the above payload.
set permit "permit:<identifier>"
# Engage/readT
curl -H $permit -X GET http://localhost:5000/Thing/god
# Engage/updateT
curl -H $permit -X PATCH http://localhost:5000/Thing/god -d '{ "description": "I am God" }'
# Engage/destroyT
curl -H $permit -X DELETE http://localhost:5000/Thing/god
```

**List**

```shell
# List/takeonT
curl -H $permit -X POST http://localhost:5000/list/Thing/god/Person -d '{ "name": "Adam" }'
curl -H $permit -X POST http://localhost:5000/list/Thing/god/Person -d '{ "name": "Eve" }'

# Takeup another - no password.
curl -X POST http://localhost:5000/Person -d '{ "name": "The Snake" }'
# List/enlistT
curl -H $permit -X PUT http://localhost:5000/list/Thing/god/Person/thesnake
# List/unlistT
curl -H $permit -X DELETE http://localhost:5000/list/Thing/god/Person/thesnake
# List/listT
curl -H $permit -X GET http://localhost:5000/list/Thing/god
# Auth/logoutT
curl  -H $permit -X DELETE http://localhost:5000/auth/Thing/god
```
