![](https://elioway.gitlab.io/eliobones/node-bones/elio-node-bones-logo.png)

> NodeJs, naked, alone, **the elioWay**

# node-bones ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Rest API in [NodeJs](https://nodejs.org/) with zero dependencies. It serves as a way to learn **NodeJs** and to "play-with" the anatomy of an **eliobones** API (needed to support applications and solutions), the **elioWay**.

**WARNING**

**node-bones** is primarily intended for learning **NodeJs**, **the elioWay**. It is not hardened to be a robust, production-ready application.

## Installing

- [Installing node-bones](https://elioway.gitlab.io/eliobones/node-bones/installing.html)

## Requirements

- [node-bones Prerequisites](https://elioway.gitlab.io/eliobones/node-bones/prerequisites.html)

## Seeing is Believing

```shell
# Start the API server.
node bones/index.js
# On another shell - run the test cycle.
node test/testClientCalls.js
```

- **See** `.data` folder for DB state after cycle has ended.
- **See** [Example Uses](examples/README.html)

## Quickstart

- [node-bones Quickstart](https://elioway.gitlab.io/eliobones/node-bones/quickstart.html)

# Credits

- [node-bones Credits](https://elioway.gitlab.io/eliobones/node-bones/credits.html)

## License

[MIT](LICENSE)

![](https://elioway.gitlab.io/eliobones/node-bones/apple-touch-icon.png)
