const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const listT = (
  THINGTYPE,
  identifier,
  permit,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.method = "GET"
  options.path = `/list/${THINGTYPE}/${identifier}`
  options.headers.permit = permit
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("listT err"))
  req.end()
}

module.exports = listT
