const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const loginT = (
  THINGTYPE,
  data,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  let loginPacket = new TextEncoder().encode(JSON.stringify(data))
  options.method = "POST"
  options.path = `/auth/${THINGTYPE}`
  options.headers["Content-Length"] = Buffer.byteLength(loginPacket)
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("loginT err"))
  req.write(loginPacket)
  req.end()
}

module.exports = loginT
