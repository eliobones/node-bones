const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const takeupT = (
  THINGTYPE,
  data,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  let takeupPacket = new TextEncoder().encode(JSON.stringify(data))
  options.method = "POST"
  options.path = `/${THINGTYPE}`
  options.headers["Content-Length"] = Buffer.byteLength(takeupPacket)
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("takeupT err"))
  req.write(takeupPacket)
  req.end()
}

module.exports = takeupT
