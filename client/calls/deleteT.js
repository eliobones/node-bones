const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const destroyT = (
  THINGTYPE,
  identifier,
  permit,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.headers.permit = permit
  options.method = "DELETE"
  options.path = `/${THINGTYPE}/${identifier}`
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("destroyT err"))
  req.end()
}

module.exports = destroyT
