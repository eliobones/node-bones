const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const readT = (
  THINGTYPE,
  identifier,
  permit,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.headers.permit = permit
  options.method = "GET"
  options.path = `/${THINGTYPE}/${identifier}`
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("readT err"))
  req.end()
}

module.exports = readT
