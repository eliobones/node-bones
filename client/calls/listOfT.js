const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const listOfT = (
  THINGTYPE,
  identifier,
  permit,
  LISTTHINGTYPE,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.method = "GET"
  options.path = `/list/${THINGTYPE}/${identifier}/${LISTTHINGTYPE}`
  options.headers = options.headers || {}
  options.headers.permit = permit
  console.log("listOfT => ", { permit, options })
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("listOfT err"))
  req.end()
}

module.exports = listOfT
