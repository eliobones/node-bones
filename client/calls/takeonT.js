const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const takeonT = (
  THINGTYPE,
  identifier,
  permit,
  LISTTHINGTYPE,
  data,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  let takeonPacket = new TextEncoder().encode(
    JSON.stringify({ ...data, mainEntityOfPage: LISTTHINGTYPE })
  )
  options.method = "POST"
  options.path = `/list/${THINGTYPE}/${identifier}/${LISTTHINGTYPE}`
  options.headers["Content-Length"] = Buffer.byteLength(takeonPacket)
  options.headers.permit = permit
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("takeonT err"))
  req.write(takeonPacket)
  req.end()
}
module.exports = takeonT
