const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const unlistT = (
  THINGTYPE,
  identifier,
  permit,
  LISTTHINGTYPE,
  listId,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.method = "DELETE"
  options.path = `/list/${THINGTYPE}/${identifier}/${LISTTHINGTYPE}/${listId}`
  options.headers.permit = permit
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("unlistT err"))
  req.end()
}

module.exports = unlistT
