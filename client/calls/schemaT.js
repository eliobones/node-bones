const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const schemaT = (
  THINGTYPE,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.method = "GET"
  options.path = `/schema/${THINGTYPE}`
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("schemaT err"))
  req.end()
}

module.exports = schemaT
