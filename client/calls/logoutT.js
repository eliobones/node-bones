const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const logoutT = (
  THINGTYPE,
  identifier,
  permit,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  options.headers.permit = permit
  options.method = "DELETE"
  options.path = `/auth/${THINGTYPE}/${identifier}`
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("logoutT err"))
  req.end()
}

module.exports = logoutT
