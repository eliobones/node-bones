const http = require("http")
const { ERROR, RESPONDER } = require("../helpers.js")

const updateT = (
  THINGTYPE,
  identifier,
  permit,
  data,
  cb,
  options = {
    hostname: "localhost",
    port: 5000,
    headers: { "Content-Type": "application/json" },
  }
) => {
  let updatePacket = new TextEncoder().encode(JSON.stringify(data))
  options.headers.permit = permit
  options.method = "PATCH"
  options.path = `/${THINGTYPE}/${identifier}`
  options.headers["Content-Length"] = Buffer.byteLength(updatePacket)
  let req = http.request(options, RESPONDER(cb))
  req.on("error", ERROR("updateT err"))
  req.write(updatePacket)
  req.end()
}

module.exports = updateT
