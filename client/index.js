const BonesClient = require("./BonesClient")
const takeonT = require("./calls/takeonT")
const destroyT = require("./calls/destroyT")
const enlistT = require("./calls/enlistT")
const listOfT = require("./calls/listOfT")
const listT = require("./calls/listT")
const loginT = require("./calls/loginT")
const logoutT = require("./calls/logoutT")
const readT = require("./calls/readT")
const schemaT = require("./calls/schemaT")
const takeupT = require("./calls/takeupT")
const unlistT = require("./calls/unlistT")
const updateT = require("./calls/updateT")

module.exports = {
  BonesClient,
  takeonT,
  destroyT,
  enlistT,
  listOfT,
  listT,
  loginT,
  logoutT,
  readT,
  schemaT,
  takeupT,
  unlistT,
  updateT,
}
