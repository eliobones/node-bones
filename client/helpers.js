const fs = require("fs")
const path = require("path")
const { StringDecoder } = require("string_decoder")
const { LOGERROR } = require("../lib")

const headers = { "Content-Type": "application/json" }
const hostname = "localhost"
const port = 5000

const stringDecoder = new StringDecoder("utf-8")

const RESPONDER = cb => {
  return res => {
    let payload = ""
    res.on("data", buffer => {
      payload += stringDecoder.write(buffer)
    })
    res.on("end", () => {
      cb(JSON.parse(payload))
    })
  }
}

const ERROR = context => {
  return err => LOGERROR(context, err)
}

const disengage = cb => {
  const dataDir = path.join(__dirname, "/../.data")
  fs.rmdir(dataDir, { recursive: true }, err => {
    fs.mkdir(dataDir, cb)
  })
}

module.exports = {
  ERROR,
  RESPONDER,
  disengage,
  headers,
  hostname,
  port,
}
