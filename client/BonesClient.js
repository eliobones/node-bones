const takeonT = require("./calls/takeonT")
const destroyT = require("./calls/destroyT")
const enlistT = require("./calls/enlistT")
const listOfT = require("./calls/listOfT")
const listT = require("./calls/listT")
const loginT = require("./calls/loginT")
const logoutT = require("./calls/logoutT")
const readT = require("./calls/readT")
const schemaT = require("./calls/schemaT")
const takeupT = require("./calls/takeupT")
const unlistT = require("./calls/unlistT")
const updateT = require("./calls/updateT")

class BonesClient {
  constructor(hostname, port) {
    this.headers = { "Content-Type": "application/json" }
    this.hostname = hostname || "localhost"
    this.port = port || 5000
    this.host = {
      hostname: this.hostname,
      port: this.port,
      headers: this.headers,
    }
  }
  destroyT(THINGTYPE, identifier, permit, cb, options = {}) {
    options = { ...this.host, ...options }
    destroyT(THINGTYPE, identifier, permit, cb, options)
  }
  enlistT(
    THINGTYPE,
    identifier,
    permit,
    LISTTHINGTYPE,
    listId,
    cb,
    options = {}
  ) {
    options = { ...this.host, ...options }
    enlistT(THINGTYPE, identifier, permit, LISTTHINGTYPE, listId, cb, options)
  }
  listOfT(THINGTYPE, identifier, permit, LISTTHINGTYPE, cb, options = {}) {
    options = { ...this.host, ...options }
    console.log("listOfT", { options })
    listOfT(THINGTYPE, identifier, permit, LISTTHINGTYPE, cb, options)
  }
  listT(THINGTYPE, identifier, permit, cb, options = {}) {
    options = { ...this.host, ...options }
    listT(THINGTYPE, identifier, permit, cb, options)
  }
  loginT(THINGTYPE, data, cb, options = {}) {
    options = { ...this.host, ...options }
    loginT(THINGTYPE, data, cb, options)
  }
  logoutT(THINGTYPE, identifier, permit, cb, options = {}) {
    options = { ...this.host, ...options }
    logoutT(THINGTYPE, identifier, permit, cb, options)
  }
  readT(THINGTYPE, identifier, permit, cb, options = {}) {
    options = { ...this.host, ...options }
    readT(THINGTYPE, identifier, permit, cb, options)
  }
  schemaT(THINGTYPE, cb, options = {}) {
    options = { ...this.host, ...options }
    schemaT(THINGTYPE, cb, options)
  }
  takeonT(
    THINGTYPE,
    identifier,
    permit,
    LISTTHINGTYPE,
    data,
    cb,
    options = {}
  ) {
    options = { ...this.host, ...options }
    takeonT(THINGTYPE, identifier, permit, LISTTHINGTYPE, data, cb, options)
  }
  takeupT(THINGTYPE, data, cb, options = {}) {
    options = { ...this.host, ...options }
    takeupT(THINGTYPE, data, cb, options)
  }
  unlistT(
    THINGTYPE,
    identifier,
    permit,
    LISTTHINGTYPE,
    listId,
    cb,
    options = {}
  ) {
    options = { ...this.host, ...options }
    unlistT(THINGTYPE, identifier, permit, LISTTHINGTYPE, listId, cb, options)
  }
  updateT(THINGTYPE, identifier, permit, data, cb, options = {}) {
    options = { ...this.host, ...options }
    updateT(THINGTYPE, identifier, permit, data, cb, options)
  }
}

module.exports = BonesClient
