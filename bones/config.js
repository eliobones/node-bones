//* Configuration settings for the site. */
const environments = {}

environments.staging = {
  httpPort: 5000,
  httpsPort: 5001,
  env: "staging",
  hashingSecret: "123456abcdef",
  host: "localhost:5000",
}

environments.production = {
  httpPort: 80,
  httpsPort: 443,
  env: "production",
  hashingSecret: "CHANGEME!",
  host: "www.elioway.com",
}

let currentEnv = process.env.NODE_ENV || "staging"

module.exports = environments[currentEnv.toLowerCase()]
