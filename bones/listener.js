//* Listens for requests to the API; parses the request object; marshalls the call to the router. */
const url = require("url")
const { StringDecoder } = require("string_decoder")
const crudifyT = require("./crudifyT")
const helpers = require("./helpers")

const stringDecoder = new StringDecoder("utf-8")

module.exports = (req, res) => {
  let header = req.headers
  var method = req.method.toLowerCase()
  let parsedUrl = url.parse(req.url, true)
  // Extract PATHs.
  let path = parsedUrl.pathname.replace(/^\/+|\/+$/g, "")
  let way = path.split("/")[0]
  let THINGTYPE = path.split("/")[1]
  let identifier = path.split("/")[2]
  let LISTTHINGTYPE = path.split("/")[3]
  let listIdentifier = path.split("/")[4]
  // Get QUERYSTRING.
  let qs = parsedUrl.query
  // Buffer request DATA.
  let packet = ""
  req.on("data", buffer => {
    packet += stringDecoder.write(buffer)
  })
  req.on("end", () => {
    packet += stringDecoder.end()
    packet = helpers.parseJsonToObject(packet)
    let handle = {
      header,
      method,
      packet,
      qs,
      identifier,
      listIdentifier,
      LISTTHINGTYPE,
      THINGTYPE,
      way,
    }
    crudifyT(handle, (status, payload) => {
      status = status || 200
      payload = typeof payload === "object" ? payload : { data: payload }
      // Send response.
      res.setHeader("Content-Type", "application/json")
      res.writeHead(status)
      res.end(JSON.stringify(payload))
      console.info(`${method.toUpperCase()} /${path} (${status})`)
    })
  })
}
