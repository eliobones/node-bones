//* A router in all but name. */
const takeonT = require("./ribs/takeonT")
const destroyT = require("./ribs/destroyT")
const enlistT = require("./ribs/enlistT")
const listT = require("./ribs/listT")
const loginT = require("./ribs/loginT")
const logoutT = require("./ribs/logoutT")
const readT = require("./ribs/readT")
const schemaT = require("./ribs/schemaT")
const takeupT = require("./ribs/takeupT")
const unlistT = require("./ribs/unlistT")
const updateT = require("./ribs/updateT")

const authRoutes = {
  post: loginT,
  delete: logoutT,
}

const engageRoutes = {
  post: takeupT,
  get: readT,
  patch: updateT,
  delete: destroyT,
}

const listRoutes = {
  post: takeonT,
  get: listT,
  put: enlistT,
  delete: unlistT,
}

const crudifyT = (handle, cb) => {
  const { way, method } = handle
  switch (way) {
    case "auth":
      authRoutes[method](handle, cb)
      break
    case "engage":
      engageRoutes[method](handle, cb)
      break
    case "list":
      listRoutes[method](handle, cb)
      break
    case "ping":
      cb(200, { Message: "Hello." })
      break
    case "schema":
      schemaT(handle, cb)
      break
    default:
      cb(404, { Error: "URL Endpoint not valid." })
  }
}

module.exports = crudifyT
