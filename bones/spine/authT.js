const engageT = require("./engageT")
const permitT = require("./permitT")

const authT = (handle, cb) => {
  engageT(handle, (exists, err, engagedData) => {
    if (exists) {
      permitT(handle, engagedData, (permitted, err) => {
        if (permitted) {
          cb(true, {}, engagedData)
        } else {
          cb(false, err)
        }
      })
    } else {
      cb(false, err)
    }
  })
}

module.exports = authT
