const PERMITLEVELS = require("../permits")
const db = require("../db")

const isGOD = (engagedData, permitAudience) => {
  /** @TODO APP.god? */
  return (
    engagedData.identifier === permitAudience ||
    engagedData.god === permitAudience
  )
}

const isLISTED = (engagedData, permitAudience) => {
  return (
    engagedData.itemList &&
    engagedData.itemList.map(i => i.indentifier).includes(permitAudience)
  )
}

const permitT = (handle, engagedData, cb) => {
  let { header, identifier, method, way, LISTTHINGTYPE } = handle
  // Route auth/delete token (aka `signoutT`) should always be level GOD.
  let permittedLevel = PERMITLEVELS.GOD
  if (way !== "auth") {
    // Permits active? Else leave at default "GOD"
    if (engagedData.hasOwnProperty("permits")) {
      // Get the permittedLevel
      permittedLevel = engagedData.permits[way][method]
      if (way === "list" && typeof permittedLevel == "object") {
        // Permission may be specific to ThingType.
        if (permittedLevel.hasOwnProperty(LISTTHINGTYPE)) {
          permittedLevel = permittedLevel[LISTTHINGTYPE]
        } else {
          // No THINGTYPE or not covered by PERMITS, resort to default.
          permittedLevel = PERMITLEVELS.GOD
        }
      }
    }
  }
  // `permitIdentifier` is the client token which was submitted.
  let { permitIdentifier } = header
  // Does the client have a token?
  if (permitIdentifier) {
    // Examine the token.
    db.read("Permit", permitIdentifier, (err, tokenData) => {
      if (!err && tokenData) {
        let { permitAudience, validUntil } = tokenData.Permit
        // Check token is still valid.
        if (validUntil > Date.now()) {
          // Check permission level + relationship of token owner to Engaged Thing:
          // - token owner is GOD to Engaged Thing OR
          // - token proves authenticated owner.
          if (
            isGOD(engagedData, permitAudience) ||
            PERMITLEVELS.AUTH === permittedLevel
          ) {
            cb(true)
          } else {
            cb(false, {
              Error: `Permission denied. ${permittedLevel} level required.`,
            })
          }
        } else {
          cb(false, { Error: "Permit not valid." })
        }
      } else {
        cb(false, { Error: "Permit not found." })
      }
    })
  } else {
    if (permittedLevel === PERMITLEVELS.ANON) {
      // then Token is not required to perform this action...
      // AKA... allow anonymous client access.
      cb(true)
    } else {
      cb(false, { Error: "No `permitIdentifier` and no anonymous access." })
    }
  }
}

module.exports = permitT
