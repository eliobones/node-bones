"use strict"

const PERMITLEVELS = {
  GOD: "GOD", // Owner of Thing can action this Thing
  LISTED: "LISTED", // Listed Things have Permission to action parent Thing
  AUTH: "AUTH", // Authenticated Things can action this Thing
  ANON: "ANON", // Anonymous Things can action this Thing
}

// Common
const Example = {
  auth: "?",
  engage: {
    post: PERMITLEVELS.ANON, // Usually you'll allow people to takeupT.
    get: PERMITLEVELS.ANON,
    patch: PERMITLEVELS.GOD,
    delete: PERMITLEVELS.GOD,
  },
  list: {
    post: PERMITLEVELS.AUTH,
    get: PERMITLEVELS.ANON,
    put: PERMITLEVELS.GOD,
    delete: PERMITLEVELS.GOD,
  },
  schema: "?",
}

module.exports = PERMITLEVELS
