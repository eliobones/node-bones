const db = require("../db")
const helpers = require("../helpers")
const authT = require("../spine/authT")

const destroyT = (handle, cb) => {
  authT(handle, (permitted, err, _) => {
    if (permitted) {
      let { identifier, THINGTYPE } = handle
      db.delete(THINGTYPE, identifier, err => {
        if (!err) {
          cb(200, { Message: `${identifier} Thing deleted.` })
        } else {
          cb(500, {
            Error: `Could not delete ${identifier} Thing.`,
            Reason: err,
          })
        }
      })
    } else {
      cb(400, err)
    }
  })
}

module.exports = destroyT
