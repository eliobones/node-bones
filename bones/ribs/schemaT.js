const schemaT = (handle, cb) => {
  let { THINGTYPE } = handle
  try {
    cb(200, require(`../../Things/${THINGTYPE}.json`))
  } catch (err) {
    cb(404, { Error: `${THINGTYPE} Schema not found.`, Reason: err })
  }
}

module.exports = schemaT
