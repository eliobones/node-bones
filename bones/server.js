//* The server and entry point to the application. */
const fs = require("fs")
const http = require("http")
const https = require("https")
const path = require("path")

const config = require("./config")
const listener = require("./listener")

const httpsDir = path.join(__dirname, "/../https")

var server = {}

const httpsServerOptions = {
  key: fs.readFileSync(`${httpsDir}/key.pem`),
  cert: fs.readFileSync(`${httpsDir}/cert.pem`),
}

server.httpServer = http.createServer(listener)
server.httpsServer = https.createServer(httpsServerOptions, listener)

server.init = () => {
  server.httpServer.listen(config.httpPort, () => {
    console.info(`HTTP Server Up: ${config.httpPort}`)
  })
  server.httpsServer.listen(config.httpsPort, () => {
    console.info(`HTTPS Server Up: ${config.httpsPort}`)
  })
}

module.exports = server
