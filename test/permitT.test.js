"use strict"
const Suite = require("node-test")
const permitT = require("../bones/spine/permitT")

const suite = new Suite("permitT Suite")

suite.test("Permitted to Access Own Record with Permit", t => {
  const permitData = () => ({
    Permit: {
      permitAudience: "tim",
      validUntil: Date.now() + 60000,
    },
  })
  let engagedData = () => ({
    identifier: "tim",
    god: "god",
    mainEntityOfPage: "Thing",
    permits: {
      engage: { post: "GOD", get: "GOD", patch: "GOD", delete: "GOD" },
      list: { post: "GOD", get: "GOD", put: "GOD", delete: "GOD" },
    },
  })

  // permitT(
  //   permitData,
  //   engagedData,
  //   "engage",
  //   "post",
  //   LISTTHINGTYPE,
  //   (perm, pack) => {
  //     t.true(result, "Allowed to post own record")
  //   }
  // )
  permitT(permitData, engagedData, "engage", "get", "Thing", (perm, pack) => {
    t.true(result, "Allowed to get own record")
  })
  permitT(permitData, engagedData, "engage", "patch", "Thing", (perm, pack) => {
    t.true(result, "Allowed to patch own record")
  })
  permitT(
    permitData,
    engagedData,
    "engage",
    "delete",
    "Thing",
    (perm, pack) => {
      t.true(result, "Allowed to delete own record")
    }
  )
})

// suite.skip("Test 2", t => {
//   throw new Error("skipped")
// })
//
// suite.todo("Test 3 - Coming Soon")
//
// suite.test("Test 4", (t, state, done) => {
//   funcWithCallback(done)
// })
//
// suite.failing("Test 5 - Need to fix this", t => {
//   t.equal(1 + 1, 3)
// })
