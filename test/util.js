const assert = require("assert")

const describe = (msg, cb) => {
  if (assert.ok(cb)) {
    console.log("☒  " + msg)
  } else {
    console.log("☑  " + msg)
  }
}

module.exports = { describe }
