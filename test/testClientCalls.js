const fs = require("fs")
const PERMITLEVELS = require("../bones/permits")
const {
  takeonT,
  destroyT,
  enlistT,
  listOfT,
  listT,
  loginT,
  logoutT,
  readT,
  schemaT,
  takeupT,
  unlistT,
  updateT,
} = require("../client")
const { disengage } = require("../client/helpers")
const { LOGCALL, LOGJOB } = require("../lib")

const password = "letmein"
const permits = {
  engage: {
    post: PERMITLEVELS.GOD, // Usually you'll allow people to takeupT.
    get: PERMITLEVELS.GOD,
    patch: PERMITLEVELS.GOD,
    delete: PERMITLEVELS.GOD,
  },
  list: {
    post: PERMITLEVELS.GOD,
    get: PERMITLEVELS.GOD,
    put: PERMITLEVELS.GOD,
    delete: PERMITLEVELS.GOD,
  },
}

const testEngageCycle = (THINGTYPE, name, deleteIt, cb) => {
  takeupT(THINGTYPE, { name, permits, password }, takeupPayload => {
    LOGCALL("takeupT", takeupPayload)

    loginT(THINGTYPE, { name, password }, tokenPayload => {
      LOGCALL("loginT", tokenPayload)

      readT(
        THINGTYPE,
        takeupPayload.identifier,
        tokenPayload.identifier,
        readPayload => {
          LOGCALL("readT", readPayload)

          updateT(
            THINGTYPE,
            takeupPayload.identifier,
            tokenPayload.identifier,
            {
              description: `${name} Description`,
            },
            updatePayload => {
              LOGCALL("updateT", updatePayload)

              if (deleteIt) {
                destroyT(
                  THINGTYPE,
                  takeupPayload.identifier,
                  tokenPayload.identifier,
                  deletePayload => {
                    LOGCALL("destroyT", deletePayload)
                    cb("Engage Cycle Complete", name)
                  }
                )
              } else {
                logoutT(
                  THINGTYPE,
                  takeupPayload.identifier,
                  tokenPayload.identifier,
                  logoutPayload => {
                    LOGCALL("logoutT", logoutPayload)

                    cb("Engage Cycle Complete", name)
                  }
                )
              }
            }
          )
        }
      )
    })
  })
}

const testListCycle = (THINGTYPE, name, cb) => {
  loginT(THINGTYPE, { name, password }, tokenPayload => {
    LOGCALL("loginT", THINGTYPE, tokenPayload)

    takeonT(
      THINGTYPE,
      tokenPayload.Permit.permitAudience,
      tokenPayload.identifier,
      "Person",
      {
        name: `${name} 1`,
        description: `${name} 1 Listed`,
        god: tokenPayload.Permit.permitAudience,
        permits,
        password,
      },
      takeonPayload => {
        LOGCALL("takeonT", takeonPayload)

        let TESTTYPES = ["CreativeWork", "Organization", "Person", "Place"]
        let takeupPromises = TESTTYPES.map((LISTTYPE, i) => {
          return new Promise(resolve => {
            takeupT(
              LISTTYPE,
              { name: `${name} ${LISTTYPE} ${i + 100}`, password },
              resolve
            )
          })
        })
        Promise.all(takeupPromises).then(takeupPayloads => {
          let enlistPromises = takeupPayloads.map(takeupPayload => {
            LOGCALL("takeupT", takeupPayload)

            return new Promise(resolve => {
              setTimeout(() => {
                enlistT(
                  THINGTYPE,
                  tokenPayload.Permit.permitAudience,
                  tokenPayload.identifier,
                  takeupPayload.mainEntityOfPage,
                  takeupPayload.identifier,
                  resolve
                )
              }, Number(takeupPayload.identifier.slice(-1)) * 300)
            })
          })

          Promise.all(enlistPromises).then(_ => {
            readT(
              THINGTYPE,
              tokenPayload.Permit.permitAudience,
              tokenPayload.identifier,
              readPayload => {
                LOGCALL("readT", readPayload)

                unlistT(
                  THINGTYPE,
                  tokenPayload.Permit.permitAudience,
                  tokenPayload.identifier,
                  "Person",
                  `${name.toLowerCase()}1`,
                  unlistPayload => {
                    LOGCALL("unlistT", unlistPayload)

                    TESTTYPES.forEach(LISTTYPE => {
                      listOfT(
                        THINGTYPE,
                        tokenPayload.Permit.permitAudience,
                        tokenPayload.identifier,
                        LISTTYPE,
                        listPayload =>
                          LOGCALL(`listOfT ${LISTTYPE}`, listPayload)
                      )
                    })

                    listT(
                      THINGTYPE,
                      tokenPayload.Permit.permitAudience,
                      tokenPayload.identifier,
                      listPayload => {
                        LOGCALL("listT", listPayload)

                        logoutT(
                          THINGTYPE,
                          tokenPayload.Permit.permitAudience,
                          tokenPayload.identifier,
                          logoutPayload => {
                            LOGCALL("logoutT", logoutPayload)

                            cb("List Cycle Complete", name)
                          }
                        )
                      }
                    )
                  }
                )
              }
            )
          })
        })
      }
    )
  })
}

disengage(() => {
  testEngageCycle("Action", "Tam", true, (msg, name) => LOGJOB(msg, name))
  testEngageCycle("Place", "Tem", true, (msg, name) => LOGJOB(msg, name))
  testEngageCycle("Thing", "Tim", false, (msg, name) => {
    LOGJOB(msg, name)
    testListCycle("Thing", name, (msg, name) => LOGJOB(msg, name))
  })
  testEngageCycle("Person", "Tom", false, (msg, name) => {
    LOGJOB(msg, name)
    testListCycle("Person", name, (msg, name) => LOGJOB(msg, name))
  })
  testEngageCycle("CreativeWork", "Tum", true, (msg, name) => LOGJOB(msg, name))
})
