const assert = require("assert")
const { describe } = require("./util")

const { randInt } = require("../lib")

describe("randInt finds its range", () => {
  let assertRange = new Set([0, 1, 2, 3, 4, 5])
  let range = new Set()
  for (let i = 0; i < 100; i++) {
    range.add(randInt(5))
  }
  assert.deepEqual(range, assertRange)
})
