const LOGCALL = (msg, payload) => {
  console.log("☑  ", msg, { payload }, "\n")
}

const LOGERROR = (msg, payload) => {
  console.log("❌  ", msg, { payload }, "\n")
}

const LOGJOB = (msg, payload) => {
  console.log("✅  ", msg, { payload }, "\n")
}

const randInt = max => Math.round(Math.random() * max)

module.exports = { LOGCALL, LOGERROR, LOGJOB, randInt }
