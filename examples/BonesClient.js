const { LOGJOB } = require("../lib")
const BonesClient = require("../client/BonesClient")
const Tasaen = require("./pizzaDelivery/FoodEstablishment/Tasaen.json")

const spine = new BonesClient("localhost", 5000)

spine.listOfT(
  "FoodEstablishment",
  Tasaen.identifier,
  "",
  "MenuItem",
  listOfPayload => {
    console.log({ listOfPayload })
    let printedMenu = listOfPayload.map(menuItem => {
      return {
        ...pick(menuItem, ["identifier", "name", "disambiguatingDescription"]),
        suitableForDiet: menuItem.MenuItem.suitableForDiet,
        offers: "£" + menuItem.MenuItem.offers.toFixed(2),
      }
    })
    LOGJOB("printedMenu", printedMenu)
  }
)
