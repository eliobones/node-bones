const { LOGJOB } = require("../lib")
const { disengage } = require("../client/helpers")

disengage(() => {
  LOGJOB("Data Deleted", "OK")
})
