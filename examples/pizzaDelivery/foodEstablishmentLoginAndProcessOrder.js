const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const loginAndProcessOrders = require("./FoodEstablishment/loginAndProcessOrders")

loginAndProcessOrders(Tasaen, loginAndProcessOrdersPayload => {
  LOGJOB(
    "FoodEstablishment loginAndProcessOrders",
    loginAndProcessOrdersPayload
  )
})
