const { pick } = require("../../../bones/helpers")
const { LOGCALL } = require("../../../lib")
const { destroyT, loginT, unlistT } = require("../../../client")

const loginAndDeletePerson = (clientUser, personId, cb) => {
  const loginPacket = pick(clientUser, ["name", "password"])
  loginT(clientUser.mainEntityOfPage, loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    if (!tokenPayload.Error) {
      destroyT("Person", personId, tokenPayload.identifier, deletePayload => {
        LOGCALL("destroyT", deletePayload)

        if (clientUser.mainEntityOfPage === "FoodEstablishment") {
          unlistT(
            "FoodEstablishment",
            clientUser.identifier,
            tokenPayload.identifier,
            "Person",
            personId,
            cb
          )
        } else {
          cb(deletePayload)
        }
      })
    } else {
      cb(tokenPayload)
    }
  })
}

module.exports = loginAndDeletePerson
