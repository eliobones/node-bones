const { pick } = require("../../../bones/helpers")
const { LOGCALL } = require("../../../lib")
const { loginT, updateT } = require("../../../client")

const loginAndUpdatePerson = (clientUser, personId, updatePacket, cb) => {
  const loginPacket = pick(clientUser, ["name", "password"])
  loginT(clientUser.mainEntityOfPage, loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    if (!tokenPayload.Error) {
      updateT("Person", personId, tokenPayload.identifier, updatePacket, cb)
    } else {
      cb(tokenPayload)
    }
  })
}

module.exports = loginAndUpdatePerson
