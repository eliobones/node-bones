const { pick } = require("../../../bones/helpers")
const { readT, enlistT, listOfT, loginT, updateT } = require("../../../client")
const { stripeT } = require("../optimize")
const emailAndEnlist = require("../EmailMessage/emailAndEnlist")
const { LOGCALL } = require("../../../lib")
const BlankOrder = require("../Order/BlankOrder.json")

const loginAndOrderAndPay = (clientUser, foodEstablishmentId, cb) => {
  const loginPacket = pick(clientUser, ["name", "password"])

  loginT(clientUser.mainEntityOfPage, loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    if (!tokenPayload.Error) {
      let clientId = clientUser.identifier
      let tokenId = tokenPayload.identifier

      // Get the Basket
      listOfT("Person", clientId, tokenId, "Order", listPayload => {
        // Check for existing Basket
        let existingBasket = listPayload.find(
          item => item.Order?.orderStatus === "Basket"
        )
        if (!existingBasket) {
          cb({
            Error: "No Basket found.",
            Message: "Login and add some food to your Basket.",
            Payload: listPayload,
          })
        } else {
          LOGCALL("existingBasket", existingBasket)

          readT(
            "Person",
            existingBasket.Order.customer,
            tokenId,
            readPersonPayload => {
              // Take the payment.
              stripeT(existingBasket, stripePayload => {
                LOGCALL("stripeT", stripePayload)

                if (stripePayload.status === "succeeded") {
                  let { Order } = existingBasket
                  Order.paymentMethodId = stripePayload.id
                  Order.paymentMethod = stripePayload.source
                  Order.paymentUrl = stripePayload.receipt_url
                  Order.orderStatus = "OrderProcessing"

                  // COmpose Email.
                  let emailMessagePacket = {
                    name: `Payment Confirmation: ${existingBasket.name}`,
                    description: `Thank you. We have received payment for your Order.\n\nComfirmationId: ${Order.paymentMethodId}.`,
                    Message: {
                      toRecipient: readPersonPayload.Person.email,
                      sender: `Tasaen Pizza <orders@tasaen.com>`,
                    },
                  }

                  emailAndEnlist(
                    emailMessagePacket,
                    clientUser,
                    tokenId,
                    emailAndEnlistPayload => {
                      // Update the status of the order.
                      updateT(
                        "Order",
                        existingBasket.identifier,
                        tokenId,
                        existingBasket,
                        updatePayload => {
                          LOGCALL("updateOrder", updatePayload)

                          // List the Order with the FoodEstablishment.
                          enlistT(
                            "FoodEstablishment",
                            foodEstablishmentId,
                            tokenId,
                            "Order",
                            updatePayload.identifier,
                            cb
                          )
                        }
                      )
                    }
                  )
                } else {
                  cb({
                    Error: "Payment Error.",
                    Message:
                      "Check the `Payload` for more details of the error",
                    Payload: stripePayload,
                  })
                }
              })
            }
          )
        }
      })
    } else {
      cb(tokenPayload)
    }
  })
}

module.exports = loginAndOrderAndPay
