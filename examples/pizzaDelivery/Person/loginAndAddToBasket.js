const { pick } = require("../../../bones/helpers")
const {
  takeonT,
  readT,
  enlistT,
  listOfT,
  loginT,
  updateT,
} = require("../../../client")
const { LOGCALL } = require("../../../lib")
const BlankOrder = require("../Order/BlankOrder.json")

const loginAndAddToBasket = (
  clientUser,
  foodEstablishmentId,
  orderItems,
  cb
) => {
  const loginPacket = pick(clientUser, ["name", "password"])

  loginT(clientUser.mainEntityOfPage, loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    if (!tokenPayload.Error) {
      let clientId = clientUser.identifier
      let tokenId = tokenPayload.identifier

      // Promise a Basket
      new Promise(resolve => {
        listOfT("Person", clientId, tokenId, "Order", listPayload => {
          LOGCALL("Person.listOfT Order", listPayload)

          // Check for existing Order/Basket
          let existingBasket = undefined
          if (Array.isArray(listPayload)) {
            let existingBasket = listPayload.find(
              item => item.Order?.orderStatus === "Basket"
            )
          }

          if (existingBasket) {
            resolve(existingBasket)
          } else {
            let orderId = `${clientId}_${Date.now()}_${foodEstablishmentId}`
            let orderDate = new Date()
            let orderPacket = {
              identifier: orderId,
              name: `${clientId}/${foodEstablishmentId}/${orderDate.toISOString()}`,
              god: clientId,
              mainEntityOfPage: "Order",
              Order: {
                orderDate,
                merchant: foodEstablishmentId,
                customer: clientId,
                offers: 0,
                paymentDue: new Date(orderDate + 60 * 1000),
                orderStatus: "Basket",
              },
              permits: BlankOrder.permits,
            }
            // Takeon an Order/Basket
            takeonT(
              "Person",
              clientId,
              tokenId,
              "Order",
              orderPacket,
              takeonPayload => {
                readT("Order", orderId, tokenId, takeonBasket => {
                  resolve(takeonBasket)
                })
              }
            )
          }
        })
      }).then(basket => {
        LOGCALL("Basket", basket)

        // Add to Basket
        let orderMenuItemsEnlistPromises = orderItems.map((menuId, i) => {
          return new Promise(resolve => {
            setTimeout(
              () =>
                enlistT(
                  "Order",
                  basket.identifier,
                  tokenId,
                  "MenuItem",
                  menuId,
                  resolve
                ),
              100 * i
            )
          })
        })
        Promise.all(orderMenuItemsEnlistPromises).then(orderedItems => {
          // View Basket and set Total Price (offers)
          listOfT(
            "Order",
            basket.identifier,
            tokenId,
            "MenuItem",
            listPayload => {
              let { Order } = basket
              Order.offers = listPayload.reduce(
                (acc, item) => acc + item.MenuItem?.offers,
                0
              )
              updateT(
                "Order",
                basket.identifier,
                tokenId,
                Order,
                updatePayload => {
                  cb(updatePayload)
                }
              )
            }
          )
        })
      })
    } else {
      cb(tokenPayload)
    }
  })
}

module.exports = loginAndAddToBasket
