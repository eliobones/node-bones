const { takeonT } = require("../../../client")
const { mailgunT } = require("../optimize")
const { LOGCALL } = require("../../../lib")

const emailAndEnlist = (emailMessagePacket, clientUser, tokenId, cb) => {
  // Send the email.
  mailgunT(emailMessagePacket, mailgunPayload => {
    LOGCALL("mailgunT", mailgunPayload)

    // Confirm send.
    if (mailgunPayload.message.includes("Queued")) {
      // Update packet
      emailMessagePacket.identifier = mailgunPayload.id
      emailMessagePacket.Message.dateSent = new Date().toISOString()
      // Save and enlist.
      takeonT(
        clientUser.mainEntityOfPage,
        clientUser.identifier,
        tokenId,
        "EmailMessage",
        emailMessagePacket,
        cb
      )
    } else {
      cb({
        Error: "Email not sent.",
        Message: emailMessagePacket,
        Payload: mailgunPayload,
      })
    }
  })
}

module.exports = emailAndEnlist
