const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const loginAndDeletePerson = require("./Person/loginAndDeletePerson")

loginAndDeletePerson(
  AdamAnt,
  AdamAnt.identifier,
  loginAndDeletePersonPayload => {
    LOGJOB("Person loginAndDeletePerson", loginAndDeletePersonPayload)
  }
)
