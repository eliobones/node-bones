const { LOGJOB } = require("../../lib")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const viewMenu = require("./FoodEstablishment/viewMenu")

viewMenu(Tasaen.identifier, viewMenuPayload => {
  LOGJOB("viewMenu", viewMenuPayload)
})
