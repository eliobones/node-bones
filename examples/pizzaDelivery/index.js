const server = require("../../bones/server")
const workers = require("./optimize/workers")

var app = {}

app.init = () => {
  server.init()
  workers.init()
}

app.init()
