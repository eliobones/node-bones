const { LOGJOB } = require("../../lib")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const takeupFoodEstablishment = require("./FoodEstablishment/takeupFoodEstablishment")

takeupFoodEstablishment(Tasaen, foodEstablishmentPayload => {
  LOGJOB(`FoodEstablishment Takeon`, foodEstablishmentPayload)
})
