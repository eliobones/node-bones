const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const loginAndUnsubscribePerson = require("./FoodEstablishment/loginAndUnsubscribePerson")

loginAndUnsubscribePerson(
  Tasaen,
  AdamAnt.identifier,
  loginAndUnsubscribePersonPayload => {
    LOGJOB(
      "FoodEstablishment loginAndUnsubscribePerson",
      loginAndUnsubscribePersonPayload
    )
  }
)
