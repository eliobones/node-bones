const { LOGJOB } = require("../../lib")
const { disengage } = require("../../client/helpers")
const AdamAnt = require("./Person/AdamAnt.json")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const loginAndAddToBasket = require("./Person/loginAndAddToBasket")
const loginAndOrderAndPay = require("./Person/loginAndOrderAndPay")
const takeupAndEnlistPerson = require("./FoodEstablishment/takeupAndEnlistPerson")
const viewMenu = require("./FoodEstablishment/viewMenu")
const { randInt } = require("../../lib")

const { MG_AUTH_RECIPIENT } = process.env
// Random Person
let names = ["Adam", "Betty", "Carol", "Dave", "Edgar", "Fran", "Greta"]
let givens = ["Ant", "Bee", "Cicada", "Dragonfly", "Earwig", "Fly", "Grub"]
let familyName = names[randInt(names.length - 1)]
let givenName = givens[randInt(givens.length - 1)]
let telephone =
  "0" +
  new Array(10)
    .fill(0)
    .map(_ => randInt(9))
    .join("")
let address = `${randInt(1000000)} ${familyName} Road, Bugton`
let email = MG_AUTH_RECIPIENT
AdamAnt.name = familyName + givenName
AdamAnt.identifier = AdamAnt.name.toLowerCase()
AdamAnt.Person = { familyName, givenName, address, email, telephone }

takeupAndEnlistPerson(
  AdamAnt,
  Tasaen.identifier,
  takeupAndEnlistPersonPayload => {
    LOGJOB("Person takeupAndEnlistPerson", takeupAndEnlistPersonPayload)

    viewMenu(Tasaen.identifier, menuPayload => {
      // Random Menu Selection.
      let orderItems = new Set()
      for (let x = 0; x < 5; x++) {
        let randomMenuItem = menuPayload[randInt(menuPayload.length - 1)]
        // Could already be in there. That's fine. Random size!
        orderItems.add(randomMenuItem.identifier)
      }

      loginAndAddToBasket(
        AdamAnt,
        Tasaen.identifier,
        [...orderItems],
        loginAndAddToBasketPayload => {
          LOGJOB("Person loginAndAddToBasket", loginAndAddToBasketPayload)

          loginAndOrderAndPay(
            AdamAnt,
            Tasaen.identifier,
            loginAndOrderAndPayPayload => {
              LOGJOB("Person loginAndOrderAndPay", { AdamAnt, orderItems })
            }
          )
        }
      )
    })
  }
)
