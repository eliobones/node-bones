# Pirple Homework Assignment #2

## Requirement

- [PirpleHomeworkAssignment2](https://www.pirple.com/courses/take/the-nodejs-master-class/assignments/9455916-homework-assignment-2)

## Nutshell

```shell
# path/to/node-bones/.env
MG_AUTH_RECIPIENT="yourAuthorisedRecipient@email.com"
MG_KEY="1234567890ABCDEF-abcdef-zyxwvu"
set MG_DOMAIN="sandbox1234567890ABCDEFabcdef.mailgun.org"
set STRIPE_SECRET="sk_test_1234567890ABCDEFabcdef"

cd examples/pizzaDelivery
node index.js
# New terminal
node busyNight.js
```

## WTF

This homework solution consumes **node-bones** which was:

1. Written to learn native **NodeJs** alongside the **Pirple** _Node.js Master Class_ course.
2. An implementation of **eliobones** architecture, an API designed, the **elioWay**.

The core principles of the **elioWay** - along with instructions about the general usage of **node-bones** - are briefly described in the [Quickstart](/docs/quickstart.html). It's not necessary you read this, but it will help explain the rather odd way I have approached this homework assignment.

## Usage

1. Set up _your_ env vars on the command line, or add to a `.env` file (recommended).

   **fish**

```shell
# path/to/node-bones/.env
set -x MG_AUTH_RECIPIENT "yourAuthorisedRecipient@email.com"
set -x MG_KEY "1234567890ABCDEF-abcdef-zyxwvu"
set -x MG_DOMAIN "sandbox1234567890ABCDEFabcdef.mailgun.org"
set -x STRIPE_SECRET "sk_test_1234567890ABCDEFabcdef"
```

**bash**

```shell
# path/to/node-bones/.env
export MG_AUTH_RECIPIENT="yourAuthorisedRecipient@email.com"
export MG_KEY="1234567890ABCDEF-abcdef-zyxwvu"
export MG_DOMAIN="sandbox1234567890ABCDEFabcdef.mailgun.org"
export STRIPE_SECRET="sk_test_1234567890ABCDEFabcdef"
```

1. Load ENVVARS and access "Homework" Folder.

```shell
cd path/to/node-bones
source .env # or load ENVVARs manually
cd examples/pizzaDelivery
```

1. Reset DB: `node ../resetDb.js`

2. Startup the **node-bones** `server` and the "processOrders" `workers`.

3. This also initialises the Pizza `FoodEstablishment` from `examples/pizzaDelivery/FoodEstablishment/Tasaen.json`

```shell
node index.js
```

Monitor terminal...

1. In a new Terminal, load ENVVARS, and access Homework Folder.

```shell
cd path/to/node-bones
source .env # or load ENVVARs manually
cd examples/pizzaDelivery
```

1. Call the scripts below.

### Homework Demo, Requirement By Requirement

```shell
cd path/to/node-bones
source .env # or load ENVVARs manually
cd path/to/node-bones/examples/pizzaDelivery
```

1. **View MenuItems** `node viewMenu.js`

2. _Lists the `FoodEstablishment` Tasaen's list of `MenuItem`s_

3. **Takeup a Person** `node personTakeupAndEnlistPerson.js`

4. _This takesup the `Person` AdamAnt from `examples/pizzaDelivery/Person/AdamAnt.json`_

5. _Adds the `Person` AdamAnt to `FoodEstablishment` Tasaen's list_

6. **Update a Person** `node updatePerson.js`

7. _Logs in `Person` AdamAnt_

8. _Updates `Person` AdamAnt's record in the DB_
9. _Logs in `FoodEstablishment` Tasaen_
10. _Updates `Person` AdamAnt's record in the DB_

11. **Login and Shop** `node personLoginAndAddToBasket.js`

12. _Logs in `Person` AdamAnt_

13. _Adds 3 Pizza `MenuItem`s to an `Order` with status "Basket"_
14. _Adds the `Order` to the AdamAnt `Person`_

15. **Login and Pay** `node personLoginAndPay.js`

16. _Logs in `Person` AdamAnt_

17. _Calls **Stripe** to make a payment._
18. _Calls **Mailgun** to Email `Person` AdamAnt a Payment Confirmation_
19. _Adds the `EmailMessage` to `Person` AdamAnt's list_
20. _Adds the `Order` to the `FoodEstablishment` Tasaen with Status "OrderProcessing"_

21. Shortly, the **processOrders** worker will fire in the background when it discovers a new `Order` which needs "OrderProcessing". For each `Order` found it will:

22. _Finds new `Order`s belonging to `FoodEstablishment` with status "OrderProcessing"_

23. _Call **Mailgun** to Email `Person` AdamAnt an Order Confirmation_
24. _Adds the `EmailMessage` to `FoodEstablishment` Tasaen's list_
25. _Changes the `Order` to Status "OrderDelivered"_

26. **Login and Unsubscribe Person** `node foodEstablishmentLoginAndUnsubscribePerson.js`

27. _Logs in `FoodEstablishment` Tasaen_

28. _Removes the `Person` AdamAnt from `FoodEstablishment` Tasaen's list_

29. **Login and Delete Person** `node personLoginAndDeletePerson.js`

30. _Logs in `Person` AdamAnt_

31. _Deletes `Person` AdamAnt from DB_

32. **Busy Night** `node busyNight.js`

33. _Randomly takesup/reuses a Person, takeson an Order, pays, etc._

34. Run it over and over! Watch **6.** refire.

### Extra Calls

- **Process Orders Manually** `node foodEstablishmentLoginAndProcessOrder.js`

  - _Logs in `FoodEstablishment` Tasaen_
  - _Finds new `Orders` with status "OrderProcessing"_

    - _Call **Mailgun** to Email `Person` an Order Confirmation_
    - _Adds the `EmailMessage` to `FoodEstablishment` Tasaen's list_
    - _Changes the `Order` to Status "OrderDelivered"_

- **Takeup FoodEstablishment Manually** `node foodEstablishmentTakeup.js`

  - _This takesup the `FoodEstablishment` Tasaen from `examples/pizzaDelivery/FoodEstablishment/Tasaen.json`_
  - _Adds 8 Pizza `MenuItem`s to `FoodEstablishment` Tasaen's list_

- **Test Stripe Call** `node stripeCall.js`

  - _Makes a simple Payment to confirm settings, connection, and that the code works._

- **Test Mailgun Call** `node mailgunCall.js`

  - _Sends a simple Email to confirm settings, connection, and that the code works._

## Credits

- <https://therootcompany.com/blog/send-email-with-mailgun-and-nodejs/>
