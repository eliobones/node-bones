const { pick } = require("../../../bones/helpers")
const { LOGCALL } = require("../../../lib")
const { takeonT, loginT, readT, takeupT } = require("../../../client")

const takeupFoodEstablishment = (foodEstablishment, cb) => {
  let foodEstablishmentPacket = pick(foodEstablishment, [
    "name",
    "alternateName",
    "description",
    "disambiguatingDescription",
    "mainEntityOfPage",
    "FoodEstablishment",
    "password",
    "permits",
  ])
  takeupT("FoodEstablishment", foodEstablishmentPacket, takeupPayload => {
    LOGCALL("takeupT", takeupPayload)

    if (!takeupPayload.Error) {
      // Login to takeup entity.
      let loginPacket = pick(foodEstablishment, ["name", "password"])
      loginT("FoodEstablishment", loginPacket, tokenPayload => {
        LOGCALL("loginT", tokenPayload)

        if (!tokenPayload.Error) {
          // Takeon the menuItems.
          let menuItemTakeonTPromises = foodEstablishment.itemList
            .filter(listItem => listItem.mainEntityOfPage === "MenuItem")
            .map((menuItemPacket, i) => {
              return new Promise(resolve => {
                // Delay ensures takeon items append to a growing list.
                setTimeout(() => {
                  takeonT(
                    "FoodEstablishment",
                    takeupPayload.identifier,
                    tokenPayload.identifier,
                    "MenuItem",
                    menuItemPacket,
                    menuItemPayload => {
                      LOGCALL("takeonT", menuItemPacket.name)
                      resolve(menuItemPacket)
                    }
                  )
                }, 100 * i)
              })
            })

          Promise.all(menuItemTakeonTPromises).then(menuItemPackets => {
            readT(
              "FoodEstablishment",
              takeupPayload.identifier,
              tokenPayload.identifier,
              cb
            )
          })
        } else {
          cb(tokenPayload)
        }
      })
    } else {
      cb({
        Error: "Unable to takeup FoodEstablishment.",
        Message: "Check your FoodEstablishment packet.",
        Payload: takeupPayload,
      })
    }
  })
}

module.exports = takeupFoodEstablishment
