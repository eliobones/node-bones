const { pick } = require("../../../bones/helpers")
const { LOGCALL } = require("../../../lib")
const { takeupT, enlistT } = require("../../../client")

const takeupAndEnlistPerson = (person, foodEstablishmentId, cb) => {
  const personPacket = pick(person, [
    "name",
    "password",
    "mainEntityOfPage",
    "Person",
    "permits",
  ])
  // This will give the FoodEstablishment permission to edit Person.
  personPacket.god = foodEstablishmentId
  // Takeup the Person and enlist them in the FoodEstablishment's list.
  takeupT("Person", personPacket, takeupPayload => {
    LOGCALL("takeupT", takeupPayload)

    enlistT(
      "FoodEstablishment",
      foodEstablishmentId,
      "",
      "Person",
      person.identifier,
      cb
    )
  })
}

module.exports = takeupAndEnlistPerson
