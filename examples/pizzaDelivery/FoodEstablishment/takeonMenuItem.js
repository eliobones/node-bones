const { pick } = require("../../../bones/helpers")
const { LOGCALL } = require("../../../lib")
const { takeonT } = require("../../../client")

const takeonMenuItem = (foodEstablishment, menuItemPacket, cb) => {
  let loginPacket = pick(foodEstablishment, ["name", "password"])
  loginT("FoodEstablishment", loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    takeonT(
      "FoodEstablishment",
      tokenPayload.Permit.permitAudience,
      tokenPayload.identifier,
      "MenuItem",
      menuItemPacket,
      cb
    )
  })
}

module.exports = takeonMenuItem
