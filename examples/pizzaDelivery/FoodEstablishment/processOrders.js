const { pick } = require("../../../bones/helpers")
const { readT, listOfT, updateT } = require("../../../client")
const emailAndEnlist = require("../EmailMessage/emailAndEnlist")
const { LOGCALL } = require("../../../lib")

const processOrders = (clientUser, tokenId, cb) => {
  // Get the Orders which need Processing
  listOfT(
    "FoodEstablishment",
    clientUser.identifier,
    tokenId,
    "Order",
    listOfOrders => {
      if (!listOfOrders.Error) {
        if (listOfOrders.length) {
          // Check for existing Basket
          let ordersNeedingProcessing = listOfOrders.filter(
            orderPacket => orderPacket.Order.orderStatus === "OrderProcessing"
          )
          if (ordersNeedingProcessing.length) {
            let orderProcessingPromises = ordersNeedingProcessing.map(
              orderPacket => {
                return new Promise(resolve => {
                  readT(
                    "Person",
                    orderPacket.Order.customer,
                    tokenId,
                    readPersonPayload => {
                      // Get the MenuList and use it to build an EmailMessage object.
                      listOfT(
                        "Order",
                        orderPacket.identifier,
                        tokenId,
                        "MenuItem",
                        listOfMenuItems => {
                          LOGCALL("listOfOrderMenuItems", listOfMenuItems)

                          if (!listOfMenuItems.Error) {
                            // Build a textual table of the order items.
                            let tableOfMenuItems = listOfMenuItems.map(
                              menuItem => {
                                return `£ ${menuItem.MenuItem.offers.toFixed(
                                  2
                                )} | ${menuItem.name} \n`
                              }
                            )
                            // Compose an Email.
                            let emailMessagePacket = {
                              name: `Confirmation of ${orderPacket.name}`,
                              description:
                                "Thank you. We have received and processed your order.\n\n" +
                                tableOfMenuItems +
                                "\n\n" +
                                "Delivery usually takes 45 minutes. We will deliver to: " +
                                readPersonPayload.Person.address,
                              Message: {
                                toRecipient: readPersonPayload.Person.email,
                                sender: `Tasaen Pizza <orders@tasaen.com>`,
                              },
                            }
                            emailAndEnlist(
                              emailMessagePacket,
                              clientUser,
                              tokenId,
                              emailAndEnlistPayload => {
                                orderPacket.Order.orderStatus = "OrderDelivered"
                                // Update the Order
                                updateT(
                                  "Order",
                                  orderPacket.identifier,
                                  tokenId,
                                  orderPacket,
                                  resolve
                                )
                              }
                            )
                          } else {
                            resolve({
                              Error: "Order incomplete",
                              Message: "No MenuItems found.",
                              Payload: orderPacket,
                            })
                          }
                        }
                      )
                    }
                  )
                })
              }
            )
            Promise.all(orderProcessingPromises).then(cb)
          } else {
            cb("No new Orders to process.")
          }
        } else {
          cb("No Orders found. Do more marketing.")
        }
      } else {
        cb(listOfOrders)
      }
    }
  )
}

module.exports = processOrders
