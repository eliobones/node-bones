const { pick } = require("../../../bones/helpers")
const { LOGCALL } = require("../../../lib")
const { loginT, unlistT } = require("../../../client")

const loginAndUnsubscribePerson = (clientUser, personId, cb) => {
  const loginPacket = pick(clientUser, ["name", "password"])
  loginT(clientUser.mainEntityOfPage, loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    if (!tokenPayload.Error) {
      unlistT(
        "FoodEstablishment",
        clientUser.identifier,
        tokenPayload.identifier,
        "Person",
        personId,
        cb
      )
    } else {
      cb(tokenPayload)
    }
  })
}

module.exports = loginAndUnsubscribePerson
