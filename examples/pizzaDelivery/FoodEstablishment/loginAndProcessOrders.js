const { pick } = require("../../../bones/helpers")
const { loginT } = require("../../../client")
const { LOGCALL } = require("../../../lib")
const processOrders = require("./processOrders")

const loginAndProcessOrders = (clientUser, cb) => {
  const loginPacket = pick(clientUser, ["name", "password"])
  loginT(clientUser.mainEntityOfPage, loginPacket, tokenPayload => {
    LOGCALL("loginT", tokenPayload)

    if (!tokenPayload.Error) {
      let tokenId = tokenPayload.identifier
      processOrders(clientUser, tokenId, cb)
    } else {
      cb(tokenPayload)
    }
  })
}

module.exports = loginAndProcessOrders
