const { pick } = require("../../../bones/helpers")
const { listOfT } = require("../../../client")

const viewMenu = (foodEstablishmentId, cb) => {
  listOfT(
    "FoodEstablishment",
    foodEstablishmentId,
    "",
    "MenuItem",
    listOfPayload => {
      let printedMenu = listOfPayload.map(menuItem => {
        return {
          ...pick(menuItem, [
            "identifier",
            "name",
            "disambiguatingDescription",
          ]),
          suitableForDiet: menuItem.MenuItem.suitableForDiet,
          offers: "£" + menuItem.MenuItem.offers.toFixed(2),
        }
      })
      cb(printedMenu)
    }
  )
}

module.exports = viewMenu
