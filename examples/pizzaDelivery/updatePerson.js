const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const loginAndUpdatePerson = require("./Person/loginAndUpdatePerson")

const { MG_AUTH_RECIPIENT } = process.env

loginAndUpdatePerson(
  AdamAnt,
  AdamAnt.identifier,
  {
    Person: {
      address: "33 Bee Bottom, Bugton",
      telephone: "04567 123 4567",
      email: MG_AUTH_RECIPIENT,
      familyName: "Ant",
      givenName: "Adam",
    },
  },
  loginAndUpdatePersonPayload => {
    LOGJOB("Person loginAndUpdatePerson", loginAndUpdatePersonPayload)

    loginAndUpdatePerson(
      Tasaen,
      AdamAnt.identifier,
      {
        Person: {
          address: "33 Cicada Close, Bugton",
          telephone: "04567 123 4567",
          email: MG_AUTH_RECIPIENT,
          familyName: "Ant",
          givenName: "Adam",
        },
      },
      loginAndUpdatePersonPayload => {
        LOGJOB(
          "FoodEstablishment loginAndUpdatePerson",
          loginAndUpdatePersonPayload
        )
      }
    )
  }
)
