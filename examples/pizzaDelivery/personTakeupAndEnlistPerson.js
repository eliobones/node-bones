const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const takeupAndEnlistPerson = require("./FoodEstablishment/takeupAndEnlistPerson")

const { MG_AUTH_RECIPIENT } = process.env

// Set the email address to a known MailGun Authorised Recipient
AdamAnt.Person.email = MG_AUTH_RECIPIENT

takeupAndEnlistPerson(AdamAnt, "tasaen", takeupAndEnlistPersonPayload => {
  LOGJOB("takeupAndEnlistPerson", takeupAndEnlistPersonPayload)
})
