const https = require("https")
const querystring = require("querystring")
const { ERROR, RESPONDER } = require("../../../client/helpers")


const { MG_KEY, MG_DOMAIN } = process.env

console.log({  MG_KEY, MG_DOMAIN })

const mailgunT = (emailMessage, cb) => {
  let { engage, name, description } = emailMessage
  let { sender, toRecipient } = engage.Message
  var mailgunPacket = new TextEncoder().encode(
    querystring.stringify({
      from: sender,
      to: toRecipient,
      subject: name,
      text: description,
    })
  )

  let options = {}
  options.method = "POST"
  options.hostname = "api.mailgun.net"
  options.port = "443"
  options.path = `/v3/${MG_DOMAIN}/messages`
  options.retry = 1
  options.auth = `api:${MG_KEY}`
  options.headers = {}
  options.headers["Content-Type"] =
    "application/x-www-form-urlencoded;charset=utf-8"
  options.headers["Content-Length"] = Buffer.byteLength(mailgunPacket)
  let req = https.request(options, RESPONDER(cb))
  req.on("error", ERROR("mailgunT err"))
  req.write(mailgunPacket)
  req.end()
}

module.exports = mailgunT
