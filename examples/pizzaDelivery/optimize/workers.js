const fs = require("fs")
const http = require("http")
const https = require("https")
const { pick } = require("../../../bones/helpers")
const { loginT } = require("../../../client")
const { LOGCALL, LOGERROR } = require("../../../lib")
const Tasaen = require("../FoodEstablishment/Tasaen.json")
const takeupFoodEstablishment = require("../FoodEstablishment/takeupFoodEstablishment")
const processOrders = require("../FoodEstablishment/processOrders")

var workers = {}

workers.tokenId = ""

workers.refreshToken = () => {
  const loginPacket = pick(Tasaen, ["name", "password"])
  loginT(Tasaen.mainEntityOfPage, loginPacket, tokenPayload => {
    if (!tokenPayload.Error) {
      workers.tokenId = tokenPayload.identifier
      LOGCALL("loginT", "Success")
    } else {
      LOGERROR({ Error: "Could not establish a token!" })
    }
  })
}

workers.processOrders = () => {
  processOrders(Tasaen, workers.tokenId, loginAndProcessOrdersPayload => {
    LOGCALL("processOrders", loginAndProcessOrdersPayload)
    if (loginAndProcessOrdersPayload.Error?.includes("Permit")) {
      workers.refreshToken()
    }
  })
}

workers.loop = () => {
  setInterval(workers.processOrders, 1000 * 10)
}

workers.init = () => {
  // Make sure the FoodEstablishment has been initialised.
  takeupFoodEstablishment(Tasaen, foodEstablishmentPayload => {
    workers.refreshToken()
    workers.loop()
  })
}

module.exports = workers
