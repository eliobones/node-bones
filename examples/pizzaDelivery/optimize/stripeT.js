const https = require("https")
const querystring = require("querystring")
const { ERROR, RESPONDER } = require("../../../client/helpers")

const { STRIPE_SECRET } = process.env

const stripeT = (basket, cb) => {
  let stripePacket = querystring.stringify({
    description: basket.name,
    amount: Math.round(basket.Order.offers * 100), // In GBP pence: smallest currency unit
    currency: "gbp",
    source: "tok_visa",
  })
  let options = {}
  options.method = "POST"
  options.protocol = "https:"
  options.hostname = "api.stripe.com"
  options.port = "443"
  options.path = `/v1/charges`
  options.headers = {}
  options.headers["Content-Type"] = "application/x-www-form-urlencoded"
  options.headers["Content-Length"] = Buffer.byteLength(stripePacket)
  options.headers.Authorization = `Bearer ${STRIPE_SECRET}`
  let req = https.request(options, RESPONDER(cb))
  req.on("error", ERROR("takeonT err"))
  req.write(stripePacket)
  req.end()
}
module.exports = stripeT
