const { LOGJOB } = require("../../lib")
const { mailgunT } = require("./optimize")

const { MG_DOMAIN, MG_AUTH_RECIPIENT } = process.env

let EmailMessage = {
  name: "Your Pizza Order",
  description: "Confirmation of your Pizza Order",
  Message: {
    toRecipient: MG_AUTH_RECIPIENT,
    sender: `Tasaen Pizza <Tasaen@${MG_DOMAIN}>`,
  },
}

mailgunT(EmailMessage, payload => {
  LOGJOB("mailgunT", payload)
})
