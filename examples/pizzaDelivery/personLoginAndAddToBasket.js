const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const loginAndAddToBasket = require("./Person/loginAndAddToBasket")

let orderItems = ["piquantpomodoro", "fieryfocaccia", "tangytaglio"]
loginAndAddToBasket(
  AdamAnt,
  Tasaen.identifier,
  orderItems,
  loginAndAddToBasketPayload => {
    LOGJOB("Person loginAndAddToBasket", loginAndAddToBasketPayload)
  }
)
