const { LOGJOB } = require("../../lib")
const AdamAnt = require("./Person/AdamAnt.json")
const Tasaen = require("./FoodEstablishment/Tasaen.json")
const loginAndOrderAndPay = require("./Person/loginAndOrderAndPay")

loginAndOrderAndPay(AdamAnt, Tasaen.identifier, loginAndOrderAndPayPayload => {
  LOGJOB("Person loginAndOrderAndPay", loginAndOrderAndPayPayload)
})
